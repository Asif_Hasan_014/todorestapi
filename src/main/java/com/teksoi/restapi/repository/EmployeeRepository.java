package com.teksoi.restapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.teksoi.restapi.model.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long>{
	
	@Modifying
    @Query("update Employee e set e.active = ?1")
    int deleteAll(Boolean active);

    List<Employee> findAllByActiveTrue();
}
