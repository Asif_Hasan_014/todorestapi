package com.teksoi.restapi.model;

import javax.persistence.Entity;

import lombok.Data;

@Entity
@Data
public class User extends BaseModel{
	
	private String username;
	
	private String password;
}
