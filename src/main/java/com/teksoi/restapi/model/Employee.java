package com.teksoi.restapi.model;

import java.util.Date;

import javax.persistence.Entity;

import lombok.Data;

@Entity
@Data
public class Employee extends BaseModel{
	
	private String name;
	
	private String email;
	
	private String phone;
	
    private String description;
    
}
