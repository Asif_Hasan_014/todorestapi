package com.teksoi.restapi.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.teksoi.restapi.dto.EmployeeDto;
import com.teksoi.restapi.dto.Response;
import com.teksoi.restapi.exception.ResourceNotFoundException;
import com.teksoi.restapi.model.Employee;
import com.teksoi.restapi.model.ToDo;
import com.teksoi.restapi.repository.EmployeeRepository;
import com.teksoi.restapi.service.CommonService;
import com.teksoi.restapi.utils.ResponseBuilder;

import javassist.NotFoundException;

@Service("commonService")
public class EmployeeServiceImpl implements CommonService {

	private static final String root = "Employee";

	private final EmployeeRepository employeeRepository;
	private final ModelMapper modelMapper;

	@Autowired
	public EmployeeServiceImpl(EmployeeRepository employeeRepository, ModelMapper modelMapper) {
		this.employeeRepository = employeeRepository;
		this.modelMapper = modelMapper;
	}

	@Override
	public <T> Response create(T data) {

		Employee employee = modelMapper.map(data, Employee.class);
		employee.setActive(true);
		employee = employeeRepository.save(employee);
		if (employee != null) {
			EmployeeDto employeeDto = new EmployeeDto();
			modelMapper.map(employee, employeeDto);
			return ResponseBuilder.getSuccessResponse(HttpStatus.CREATED, employeeDto,
					String.format("Created successfully", root));
		}
		return ResponseBuilder.getFailResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Internal error occurred");
	}

	@Override
	public Response getAll() {
		try {
			List<Employee> employeeList = employeeRepository.findAllByActiveTrue();
			if (employeeList.isEmpty()) {
				throw new NotFoundException("Data is not found");
			}
			List<EmployeeDto> employeeDtos = new ArrayList<>();
			employeeList.forEach(course -> {
				EmployeeDto employeeDto = modelMapper.map(course, EmployeeDto.class);
				employeeDtos.add(employeeDto);
			});

			return ResponseBuilder.getSuccessResponse(HttpStatus.OK, employeeDtos, employeeDtos.size(),
					String.format("%s list", root));
		} catch (NotFoundException e) {
			return ResponseBuilder.getFailResponse(HttpStatus.NOT_FOUND, e.getMessage());
		} catch (Exception e) {
			return ResponseBuilder.getFailResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Internal error occurred");
		}
	}

	@Override
	public <T> Response update(Long id, T data) {
		Optional<Employee> optionalEmployee = employeeRepository.findById(id);
		if (!optionalEmployee.isPresent()) {
			return ResponseBuilder.getFailResponse(HttpStatus.NOT_FOUND,
					String.format("Requested %s could not be found", root));
		}

		try {
			Employee employee = optionalEmployee.get();
			modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
			modelMapper.map(data, employee);
			employee = employeeRepository.save(employee);

			if (employee != null) {
				EmployeeDto employeeDto = new EmployeeDto();
				modelMapper.map(employee, employeeDto);
				return ResponseBuilder.getSuccessResponse(HttpStatus.OK, employeeDto,
						String.format("%s updated successfully", root));
			}
			return ResponseBuilder.getFailResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Internal error occurred");

		} catch (NullPointerException e) {
			return ResponseBuilder.getFailResponse(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

	@Override
	public Response delete(Long id) {
		Optional<Employee> optionalEmployee = employeeRepository.findById(id);

		if (!optionalEmployee.isPresent()) {
			return ResponseBuilder.getFailResponse(HttpStatus.NOT_FOUND,
					String.format("Requested %s could not be found", root));
		}

		try {
			Employee employee = optionalEmployee.get();
			employee.setActive(false); // Soft delete by setting active to false
			employee = employeeRepository.save(employee);

			if (employee != null) {
				return ResponseBuilder.getSuccessResponse(HttpStatus.OK, null,
						String.format("%s deleted successfully", root));
			}
			return ResponseBuilder.getFailResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Internal error occurred");
		} catch (ResourceNotFoundException e) {
			return ResponseBuilder.getFailResponse(HttpStatus.NOT_FOUND, e.getMessage());
		} catch (NullPointerException e) {
			return ResponseBuilder.getFailResponse(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

	@Override
	public Response deleteAll() {
		try {
			employeeRepository.deleteAll(false);
			return ResponseBuilder.getSuccessResponse(HttpStatus.OK, null,
					String.format("%s deleted successfully", root));
		} catch (Exception e) {
			return ResponseBuilder.getFailResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Internal error occurred");
		}
	}

	@Override
	public <T> Response getById(Long id) {
		Optional<Employee> optionalEmployee = employeeRepository.findById(id);

		if (!optionalEmployee.isPresent()) {
			return ResponseBuilder.getFailResponse(HttpStatus.NOT_FOUND,
					String.format("Requested %s could not be found", root));
		}

		try {
			Employee employee = optionalEmployee.get();

			if (employee != null) {
				EmployeeDto employeeDto = new EmployeeDto();
				modelMapper.map(employee, employeeDto);
				return ResponseBuilder.getSuccessResponse(HttpStatus.OK, employeeDto,
						String.format("%s deleted successfully", root));
			}
			return ResponseBuilder.getFailResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Internal error occurred");
		} catch (NullPointerException e) {
			return ResponseBuilder.getFailResponse(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

}
