package com.teksoi.restapi.service;

import com.teksoi.restapi.dto.Response;
import com.teksoi.restapi.dto.ToDoDto;

public interface CommonService {
	
	<T> Response create(T data);

	Response getAll();
	
	<T> Response getById(Long id);

	<T> Response update(Long id, T data);

	Response delete(Long id);

	Response deleteAll();
	
}
