package com.teksoi.restapi.controller;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.OPTIONS;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.teksoi.restapi.dto.EmployeeDto;
import com.teksoi.restapi.dto.Response;
import com.teksoi.restapi.dto.ToDoDto;
import com.teksoi.restapi.service.CommonService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api")
public class EmployeeController {

	private final CommonService commonService;

	@Autowired
	public EmployeeController(CommonService commonService) {
		this.commonService = commonService;
	}
	
	@PostMapping("/employee")
    @ResponseBody
    public Response create(@RequestBody EmployeeDto employeeDto, HttpServletResponse httpServletResponse, HttpServletRequest request) {
        Response response = commonService.create(employeeDto);
        httpServletResponse.setStatus(response.getStatusCode());
        return response;
    }

    @GetMapping("/employee")
    @ResponseBody
    public Response getAll(HttpServletResponse httpServletResponse) {//To make it simple pagination has not been implemented

        Response response = commonService.getAll();
        httpServletResponse.setStatus(response.getStatusCode());
        return response;
    }
    
    @GetMapping("/employee/{id}")
    @ResponseBody
    public Response getById(@PathVariable("id") Long id, HttpServletResponse httpServletResponse, HttpServletRequest request) {
        Response response = commonService.getById(id);
        httpServletResponse.setStatus(response.getStatusCode());
        return response;
    }

    @PutMapping("/employee/{id}")
    @ResponseBody
    public Response update(@PathVariable("id") Long id,@RequestBody EmployeeDto employeeDto, HttpServletResponse httpServletResponse, HttpServletRequest request) {
        Response response = commonService.update(id, employeeDto);
        httpServletResponse.setStatus(response.getStatusCode());
        return response;
    }

    @DeleteMapping("/employee/{id}")
    @ResponseBody
    public Response delete(@PathVariable("id") Long id, HttpServletResponse httpServletResponse, HttpServletRequest request) {
        Response response = commonService.delete(id);
        httpServletResponse.setStatus(response.getStatusCode());
        return response;
    }

    @DeleteMapping("/employee")
    @ResponseBody
    public Response delete(HttpServletResponse httpServletResponse) {
        Response response = commonService.deleteAll();
        httpServletResponse.setStatus(response.getStatusCode());
        return response;
    }

}
