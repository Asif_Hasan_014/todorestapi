package com.teksoi.restapi.dto;

import java.util.Date;

import lombok.Data;

@Data
public class EmployeeDto {
	
	private Long id;
	
	private String name;

	private String email;

	private String phone;

	private String description;
}
