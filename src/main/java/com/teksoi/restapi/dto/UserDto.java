package com.teksoi.restapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.teksoi.restapi.model.User;

import lombok.Data;

@Data
public class UserDto {
	
	private String username;
	
	private String password;
	
}
